#include<iostream>

using namespace std;

struct node{
	int data;
	node *next;
};

node *createNode(int x){
    node *temp = new node;
    temp->next = NULL;
    temp->data = x; 
    return temp;
}

void printList(node *l){
	node *p = l;
	while (p != NULL){
		cout << p->data << " ";
		p = p->next;
	}
}

node *addElement(node*p, int x){
	node *temp = createNode(x);
	p->next = temp;
	return temp;
}

int searchNode(node* h, int k){
    node* tmp = h;
    while (tmp->next != nullptr && k>=0){
        tmp= tmp->next;
        k-=1;
    }
    try{
        return tmp->data;
    }
    catch(exception e){
        cout<<"Out of range!";
    }
}
int main(){
	int n, x;
	cin >> n;
	cin >> x;
	node *l = createNode(x);
	node *p = l;
	for (int i = 1; i < n; i++){
		cin >> x;
		p = addElement(p, x);
	}
	int k;
    cin>>k;
    cout<<searchNode(p, k);
}